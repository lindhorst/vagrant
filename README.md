# Vagrant

# Contents of the repo

1. Resources for creating Virtualbox VMs
2. Tips for working with vagrant
3. Instructions for making custom Vagrant boxes

# Create Virtualbox VMs

## Setup vagrant directory

Copy the `template` directory to a work location for the vm cluster. We'll call
the destination the "vagrant directory". For example:
  
  ```bash
  cp -r template demo
  cd demo
  ```

## Provision VMs

- Work from the vagrant directory

- Copy one of the Vagrantfile templates in directory `vagrantfiles` as
  `Vagrantfile` to the vagrant directory. For example:
  
  ```bash
  cp vagrantfiles/Vagrantfile-multi ./Vagrantfile
  ```
  
- Edit `Vagrantfile` to configure your vm(s) as desired

- Provision the vm(s) from within the vagrant directory with the command:

``` bash
vagrant up
```

## Log into VMs

Log into the vm(s) with one of the following methods:

- From the vagrant directory:

```bash
vagrant ssh <node-name>
```

- From any directory, using the vagrant application, where <vm-id> is the
  hex id of the vm listed with `vagrant global-status`:

```bash
vagrant ssh <vm-id>
```

- From any directory, using regular ssh. For the port number, use the value
  configured int he Vagrantfile. (note that this works only because we are
  storing the current user's public key in the vm(s)):

```bash
ssh -p <forwarded-port> vagrant@localhost
```

- If the network type of the VMs is "hostonly", another option to login is
  through the standard port 22, using the IP of the VMs:
  
``` bash
ssh vagrant@<ip-address>
```

## Add ansible user

This section needs to be double-checked.

- Work from the vagrant directory

- Edit file `environments/virtualbox/hosts` with values from your VM definitions:

	- Choose whether to ssh into the VMs by IP over port 22 or through localhost
      and port forwarding. If the network type of the VMs is "natnetworking,"
      you have to use localhost and port forwarding. If the network type
      "hostonly", choose either method.

	- Edit the IP addresses or server names and port numbers, depending on the
      chosen connection method

	- Edit the path to your public key
	
- Run the playbook to create the ansible user as user `vagrant` with a command
  of this form:

``` bash
ansible-playbook -i environments/virtualbox/hosts -u vagrant playbook-add-ansible-user.yml
```

Now you can ssh into the VMs as user ansible and run additional ansible playbooks.

``` bash
ssh -p <forwarded-port> ansible@localhost
```

- If the network type of the VMs is "hostonly", another option to login is
  through the standard port 22, using the IP of the VMs:
  
``` bash
ssh ansible@<ip-address>
```

## References

Erik's vagrant setup
https://childrens-atlassian/bitbucket/projects/EADO/repos/ansible-vagrant-sch-centos7/browse



# Tips for working with vagrant

## vagrant command

User command `vagrant` for all operations. Look at what's available by running:

``` bash
vagrant help
```

## global status

The following will show a list of existing vagrant vms. The hex ids are useful
for manipulating vms directly.

``` bash
vagrant global-status
```

## get login info

The following displays useful information about a vm, including the ssh port
that gets forwarded and the path to the private key used by the vagrant user:

``` bash
vagrant ssh-config <id>
```

# Instructions for making custom vagrant boxes

The method did not work completely. The boxes I created appeared to be working,
but were giving strange errors and lags during provisioning.

See subdirectory `make-vagrant-box` for what I did.
