2020-02-25 Vagrant base box for CentOS 7

# 2020-02-25 Vagrant base box for CentOS 7 (updated 2020-05-28)

This is a series of minimal CentOS 7 boxes, which correspond to particular iso images. The steps to create vagrant boxes are described.

## References

https://www.vagrantup.com/docs/boxes/base.html
https://www.vagrantup.com/docs/virtualbox/boxes.html

## Variables used in the instructions

The instructions include several variables used for generality. They are not shell environment variables, but intended for readability.

### lindhorst/76

- `<vm-name>` = lindhorst-76
- `<box-version>` = 1.1
- `<iso>` = CentOS-7-x86_64-Minimal-1810.iso
- `<mac>` = 0800275421A1
- `<cloud-name>` = lindhorst/cent76

### lindhorst/77

- `<vm-name>` = lindhorst-77
- `<box-version>` = 1.1
- `<iso>` = CentOS-7-x86_64-Minimal-1908.iso
- `<mac>` = 08002749A1A0
- `<cloud-name>` = lindhorst/cent77

### lindhorst/78

- `<vm-name>` = lindhorst-78
- `<box-version>` = 1.1
- `<iso>` = CentOS-7-x86_64-Minimal-2003.iso
- `<mac>` = 080027831931
- `<cloud-name>` = lindhorst/cent78


## Make a Virtualbox VM manually

### Preparation

Work on the Rocket-nano SDD attached to the SCH macbook.

Download `<iso>`.

In the VirtualBox preferences, set the default machine folder to `/Volumes/ROCKET-nano/virtualbox-vms`.

### New VM

Create a new VM called `<vm-name>`:

![3e78d41c53864c0c868e2059dcef55b5.png](../_resources/714142b49a2f46c89ec39957547ad771.png)

Set memory to 1024 MB (setting it too small crashes the installation):

![d5281688a35992327b8bfaf61fac3dea.png](../_resources/8bbc4fc461dc433cae45588d753c79ba.png)

Create virtual disk: vdi, dynamically allocated, 8 GB, named `root.vdi`

![911ccd6cc8fbf8b363b744b8c30606ad.png](../_resources/3cc6ce5352b34bd78b519b7583b099ff.png)

![0bbd193d9a4093fb552bb40265147d83.png](../_resources/d13fb1bb40784dd0951b35f702994428.png)

![b00f8dbdfc2f20bccf10ef8e01ec8190.png](../_resources/0e2ca5e395734d8a8a76a9873f7b1a98.png)

![488d1b61a0484e57c53b82ded78ff1a7.png](../_resources/29a011a01a6f4ee3bd34d0728d424958.png)

### Change settings

#### General:

![629d03c7e059e5aeea5237aadb7a67f9.png](../_resources/6f5e4ede040946e7ae4d981ccaee3b21.png)

![f7152db2a3e7d41d1510106e9544400e.png](../_resources/539ebca351b84ec3a1e06cefc2d75e20.png)

![9f5bc766300af71c09e96b1d9218d474.png](../_resources/3654b24bd8b249a2b493b7fd3a214788.png)

![27e4a04386c3d0b70d113fd24314712d.png](../_resources/3cd463556f3a41f3a1666f6c8b3cc10a.png)


#### System:

![19edeb916650cc2e6400345dd94f11e3.png](../_resources/b51cf124d05648148dbbc9b4630eecc6.png)

![0ef6882ea60f5269b44e18b58661ca79.png](../_resources/8bf640a4fa2f4f5086671c821c27da5c.png)

![c550893b90001c411a8498066c626a16.png](../_resources/1184ed484b7842868ad6de294f2cde07.png)

#### Display:

![96d4741056ccb246ba1d0cf4d94f7927.png](../_resources/1af3518250d74c8ab72d55dec317765c.png)

![8272415faf997274bf0f4c597f2dd51e.png](../_resources/e48f70a9bde548d7ae0e7db615243671.png)

![276fb82bb287cbca0438839cd08e63c0.png](../_resources/738be6bacd7a44ec8aef106c7cbd47a2.png)

#### Storage:
Add a 500 MB volume, vdi, dyn, called swap.vdi

![a5707cbcc522241edbf61aacd2aa7e5b.png](../_resources/c9a82027def341d4915e138ba6fd222b.png)

![8fe033fdfa6699ebca7ba904eaefc8d9.png](../_resources/e6e6a684c0b24da58b0c7d04e0f6fc36.png)

Mount iso `<iso>`

![4f23e413c242e911fcb1662b8f6d499f.png](../_resources/a59a1387f277440e95d96c95aef124c1.png)

![9ff36a4dddfffdc321061be247a5475a.png](../_resources/3f16f47c04af4d88a7070a0ebf377b93.png)

#### Audio:
Disable:

![579620313661264b929c9bc71f4c0b75.png](../_resources/fe4feb56f9e64d29a0a0f498b9fe7659.png)

#### Network:
Read MAC address for NAT adapter. Save value in variable `<mac>`

![414e3b6f1be85ca63f76412cf93e3e29.png](../_resources/b076f43c9be64702a62dfd3b0cc98237.png)

#### Ports:

Leave all serial ports disabled.

![2db874be5fb56a0d6aea62183ebe5d5a.png](../_resources/93f322a0fc2344ec89f58ed65640fc01.png)

Disable USB controller

![5a6e2de67509a7c207ec66bf4398d11b.png](../_resources/28397c6536fb4c95b743f5ece60a3d6f.png)

#### Shared Folders:

Do not share any folders.

![d61c0a7ad5ad42085c053bcdefd68e27.png](../_resources/b69f046359564afa9517ec26ef923f6a.png)

### Boot and install CentOS

- data & time: Americas/Los Angeles
- disk:
  - select both disks
  - "I will configure partitioning"
    - new moint points will use the following partition scheme: standard
    - /boot - 250 MiB, xfs, /dev/sda1
    - /  - remaining (7941 MiB), xfs, /dev/sda2
    - swap - all (499 MiB), swap, /dev/sdb1
- network: turn on ethernet (enp0s3) Read the IP address, it is 10.0.2.15.
- what is kdump? it is enabled by default. Leave it on.
- root password: vagrant

If I forgot to read the ip address, log in as root and read it with command

```bash
ip add
```

it is 10.0.2.15/24 for enp0s3

### Configure ssh from host (temporarily)

Shut down, add the following port forwarding rule in network adapter 1, nat:

- name: ssh
- protocol: tcp
- host ip: 127.0.0.1
- host port: 2222
- guest ip: 10.0.2.15
- guest port: 22

![38a727007ff5f9d0808acf7d42e8ed6c.png](../_resources/2854db06867b4def8b0a6d659c2a0369.png)

Start the VM and ssh into it from a terminal window with:

```bash
ssh -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" -p 2222 root@127.0.0.1
```

## Add vagrant user

Work logged in as root as above.

Add the `vagrant` user with password: vagrant:

```bash
adduser vagrant && (echo 'vagrant:vagrant' | chpasswd)
```

Add the insecure public key:

```bash
su - vagrant
```

```bash
mkdir -p ~vagrant/.ssh
cat > ~vagrant/.ssh/authorized_keys <<'endkey'
ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEA6NF8iallvQVp22WDkTkyrtvp9eWW6A8YVr+kz4TjGYe7gHzIw+niNltGEFHzD8+v1I2YJ6oXevct1YeS0o9HZyN1Q9qgCgzUFtdOKLv6IedplqoPkcmF0aYet2PkEDo3MlTBckFXPITAMzF8dJSIFo9D8HfdOV0IAdx4O7PtixWKn5y2hMNG0zQPyUecp4pzC6kivAIhyfHilFR61RGL+GPXQ2MWZWFYbAGjyiYJnAmCP3NOTd0jMZEnDkbUvxhMmBYSdETk1rRgm+R4LOzFUGaHqHDLKLX+FIPKcF96hrucXzcWyLbIbEgE98OHlnVYCzRdK8jlqm8tehUc9c9WhQ== vagrant insecure public key
endkey
chmod 600 .ssh/authorized_keys
chmod 700 .ssh

exit
```

Configure passwordless sudo for entire vagrant group.
As root:
```bash
echo '%vagrant ALL=(ALL) NOPASSWD: ALL' > /etc/sudoers.d/vagrant
```

Shut down the VM.

In Settings -> Network, remove the port forwarding rule:

![921710119186d22e47bd2472793891cf.png](../_resources/1a55737af48d4dbd903d0f5ce12d1858.png)

Take a snapsho:

![7cb55471c66e70a0debc49879b5d51eb.png](../_resources/bc8a2285f8f34e76a948a68cd86ad5f4.png)

## Make Vagrant box

Work in directory `08.computing-systems/22.virtualbox-vms/cent7-sch`

```bash
cd /Users/phodor/Desktop/data/08.computing-systems/22.virtualbox-vms/cent7-sch
```

### Package the box file

Make an additional Vagrantfile to be included in the box. Its role is to rsync on boot the host directory containing the Vagrantfile with guest directory `/vagrant`.

Contents of additional Vagrantfile `Vagrantfile-for-package`:

```ruby
Vagrant.configure("2") do |config|
  config.vm.synced_folder ".", "/vagrant", type: "rsync"
end
```

Set environment variables, replace variable values:

```bash
export VM_NAME="<vm-name>"
export BOX_VERSION="<box-version>"
```

Execute the package command:

```bash
vagrant package --base $VM_NAME --vagrantfile ./Vagrantfile-for-package --output "./${VM_NAME}.${BOX_VERSION}.box"
```

Get the md5 hash:

```bash
md5 "./${VM_NAME}.${BOX_VERSION}.box" > "./${VM_NAME}.${BOX_VERSION}.box.md5"
```

## Test the box file

Work in directory `08.computing-systems/22.virtualbox-vms/cent7-sch`

```bash
cd /Users/phodor/Desktop/data/08.computing-systems/22.virtualbox-vms/cent7-sch
```

Set environment variables, replace variable values:

```bash
export VM_NAME="<vm-name>"
export BOX_VERSION="<box-version>"
```

On the host:

```bash
vagrant box add --name "${VM_NAME}/${BOX_VERSION}" "./${VM_NAME}.${BOX_VERSION}.box"

mkdir ./test.${VM_NAME}
cd ./test.${VM_NAME}
vagrant init "${VM_NAME}/${BOX_VERSION}"
echo cucu > cucu
vagrant up
vagrant ssh
```

On the guest:

```
[vagrant@localhost ~]$ cat /vagrant/cucu
cucu
[vagrant@localhost ~]$ sudo cat /etc/sudoers.d/vagrant
%vagrant ALL=(ALL) NOPASSWD: ALL
[vagrant@localhost ~]$ exit
logout
Connection to 127.0.0.1 closed.
```

Cleanup:

```bash
vagrant destroy
vagrant box remove "${VM_NAME}/${BOX_VERSION}"
cd ..
rm -r ./test.${VM_NAME}
```

Note: keep the VM `<vm-name>` for creating other, derived boxes.

## Upload box to Vagrant Cloud

Make an account on Vagrant Cloud at https://app.vagrantup.com/.

Click "New Vagrant Box", fill the required info, and upload the box file when prompted.

The name of the box is `<cloud-name>`, example: "[lindhorst/cent77](https://app.vagrantup.com/lindhorst/boxes/cent77)".


## Smaller versions of the boxes

These Vagrant boxes are smaller versions of the base boxes created straight from the minimal CentOS isntallation. They were made by removing certain driver packages that are unlikely to be useful in a Virtualbox VM. The list of packages that was removed was determined by looking at installed packages on a centos/7 box.

### Compare list of packages

Work in directory `08.computing-systems/22.virtualbox-vms/01.compare-erik-paul-vagrant`

Create a couple of vms, one based on centos/7, the other on lindhorst/cent77

Run the following command on each, to generate lists of installed RPM packages:

```bash
rpm -qa | sort > list-rpm-<box>-sorted
```

Copy the 2 lists to the macbook and compare them with:

```bash
diff list-rpm-centos-sorted list-rpm-lindhorst-sorted | less
```

### Remove packages

Log into each of the Virtualbox vms and rmove RPM packages:

```bash
yum autoremove aic94xx-firmware
yum autoremove alsa-*
yum remove centos-logos plymouth*
yum remove ivtv-firmware
yum remove iwl*
yum remove snappy
```

Create a snapshot labeled "removed packages".

### Package box files

Work in directory `/Users/phodor/Desktop/data/08.computing-systems/22.virtualbox-vms/cent7-sch`.

```bash
export VM_NAME="<vm-name>"
export BOX_VERSION="<box-version>"
```

```bash
vagrant package --base $VM_NAME --vagrantfile ./Vagrantfile-for-package --output "./${VM_NAME}s.${BOX_VERSION}.box"

md5 "./${VM_NAME}s.${BOX_VERSION}.box" > "./${VM_NAME}s.${BOX_VERSION}.box.md5"
```

## Upload box to Vagrant Cloud

Click "New Vagrant Box", fill the required info, and upload the box file when prompted.

The name of the boxes are `<cloud-name>s`, example: "[lindhorst/cent77s](https://app.vagrantup.com/lindhorst/boxes/cent77s)".

